import React, {Component} from 'react';
import {connect} from "react-redux";
import NumberKey from "../components/common/numberKey";
import * as actions from '../actions';
import * as CONSTS from '../constants';

class HomePage extends Component {

    constructor() {
        super();
    }


    setNum = (num) => {
        if (this.props.calculatorData.showNum !== "") {
            this.props.dispatch(actions.setNumberAction(num))
        }

    };

    setFloatTrue() {
        this.props.dispatch(actions.setFloatTrueAction(true))
    }

    setClear() {
        this.props.dispatch(actions.resetCalcAction())
    }


    turnOffCalculator = () => {
        this.props.dispatch(actions.turnOffAction())
    };

    operationOn(operation) {
        if (this.props.calculatorData.showNum !== "") {
            this.getResult();
            this.props.dispatch(actions.operationOnAction(operation));
        }
    }

    setMemory = (type) => {
        if (this.props.calculatorData.showNum !== "") {
            this.props.dispatch(actions.setMemoryAction(type));
        }
    };

    reverseVal = () => {
        if (this.props.calculatorData.showNum !== "") {
            this.props.dispatch(actions.reverseNumberAction());
        }
    };

    getResult = () => {

        if (this.props.calculatorData.firstNum !== "") {
            let result = null;
            switch (this.props.calculatorData.operation) {
                case CONSTS.PLUS:
                    console.log(parseFloat(this.props.calculatorData.result), parseFloat(this.props.calculatorData.showNum))
                    result = parseFloat(this.props.calculatorData.result) + parseFloat(this.props.calculatorData.showNum);
                    break;
                case CONSTS.MINUS:
                    result = parseFloat(this.props.calculatorData.result) - parseFloat(this.props.calculatorData.showNum);
                    break;
                case CONSTS.CROSS:
                    result = parseFloat(this.props.calculatorData.result) * parseFloat(this.props.calculatorData.showNum);
                    break;
                case CONSTS.DIVIDE:
                    result = parseFloat(this.props.calculatorData.result) / parseFloat(this.props.calculatorData.showNum);
                    break;

                case CONSTS.MOD:
                    result = parseFloat(this.props.calculatorData.result) % parseFloat(this.props.calculatorData.showNum);
                    break;
                default:
                    break;

            }
            this.props.dispatch(actions.setResultAction(result))
        }

    };

    calculateRoot = () => {
        let result = Math.sqrt(this.props.calculatorData.showNum);
        this.props.dispatch(actions.setResultAction(result))
    };


    resetMemory = () => {
        this.props.dispatch(actions.resetMemoryAction());
    };

    turnOnCalculator = () => {
        this.props.dispatch(actions.turnOnCalculatorAction());
    };

    reloadMemory = () => {
        this.props.dispatch(actions.reloadMemoryAction());
    };

    render() {

        return (
            <main className='machine-body'>

                {/* solar block*/}
                <div className='top-part'>

                    <div className='trademark'>
                        <img src="https://www.casio-intl.com/assets/logos/casio-logo.svg" alt="CASIO LOGO"/>
                    </div>

                    <div className='solar-panel'>
                        <div className='solar-cells-block'>
                            <div className='solar-cells-item'></div>
                            <div className='solar-cells-item'></div>
                            <div className='solar-cells-item'></div>
                            <div className='solar-cells-item'></div>
                        </div>

                        <div className='solar-description'>
                            <span>
                                TWO WAY POWER
                            </span>
                        </div>
                    </div>
                </div>

                {/*screen block*/}
                <div className='screen-panel'>
                    <div className='screen-frame'>
                        <div className='led-screen'>

                            <div className='left-part'>

                            </div>

                            <div className='right-part'>
                                <span> {this.props.calculatorData.showNum}</span>
                            </div>

                        </div>
                    </div>
                </div>

                {/*keypad block*/}
                <div className='keypad-panel'>

                    <div className='panel'>

                        <div className='row-buttons'>
                            <div className='model-no'>
                                <span>SL-300SV</span>
                            </div>

                            <div className='btn-block'>

                                <div className='btn-item'>
                                    <button onClick={() => {
                                        this.calculateRoot()
                                    }}>
                                        <span>√</span>
                                    </button>
                                </div>


                                <div className='btn-item'>
                                    <button onClick={() => {
                                        this.turnOffCalculator();
                                    }}>
                                        <span>off</span>
                                    </button>
                                </div>

                            </div>

                        </div>

                        <div className='center-part'>
                            <div className='row-buttons'>
                                <div className='btn-item'>
                                    <button className='keypad-button' onClick={() => {
                                        this.resetMemory()
                                    }}>MC
                                    </button>
                                </div>

                                <div className='btn-item'>
                                    <button className='keypad-button' onClick={() => {
                                        this.reloadMemory();
                                    }}>MR
                                    </button>
                                </div>

                                <div className='btn-item'>
                                    <button className='keypad-button' onClick={() => {
                                        this.setMemory(CONSTS.MINUS)
                                    }}>M-
                                    </button>
                                </div>

                                <div className='btn-item'>
                                    <button className='keypad-button' onClick={() => {
                                        this.setMemory(CONSTS.PLUS)
                                    }}>M+
                                    </button>
                                </div>

                                <div className='btn-item'>
                                    <button className='keypad-button' onClick={() => {
                                        this.operationOn(CONSTS.DIVIDE)
                                    }}>/
                                    </button>
                                </div>

                            </div>

                            <div className='column-buttons'>

                                <div className='btn-item'>
                                    <button className='keypad-button' onClick={() => {
                                        this.operationOn(CONSTS.CROSS)
                                    }}>X
                                    </button>
                                </div>

                                <div className='btn-item'>
                                    <button className='keypad-button' onClick={() => {
                                        this.operationOn(CONSTS.MINUS)
                                    }}>-
                                    </button>
                                </div>


                                <div className='btn-item'>
                                    <button className='keypad-button t-2' onClick={() => {
                                        this.operationOn(CONSTS.PLUS)
                                    }}>+
                                    </button>
                                </div>

                            </div>

                            <div className='number-buttons'>
                                <NumberKey num={7} setNum={this.setNum}/>
                                <NumberKey num={8} setNum={this.setNum}/>
                                <NumberKey num={9} setNum={this.setNum}/>

                                <NumberKey num={4} setNum={this.setNum}/>
                                <NumberKey num={5} setNum={this.setNum}/>
                                <NumberKey num={6} setNum={this.setNum}/>

                                <NumberKey num={1} setNum={this.setNum}/>
                                <NumberKey num={2} setNum={this.setNum}/>
                                <NumberKey num={3} setNum={this.setNum}/>


                                <NumberKey num={0} setNum={this.setNum}/>

                                <div className="btn-item">
                                    <button className='keypad-button' onClick={() => {
                                        this.setFloatTrue()
                                    }}>.
                                    </button>
                                </div>


                                <div className="btn-item">
                                    <button className='keypad-button' onClick={() => {
                                        this.getResult()
                                    }}>=
                                    </button>
                                </div>

                            </div>

                            <div className='column-buttons t-2'>
                                <div className='btn-item'>
                                    <button className='keypad-button' onClick={() => {
                                        this.operationOn(CONSTS.MOD)
                                    }}>%
                                    </button>
                                </div>

                                <div className='btn-item'>
                                    <button className='keypad-button' onClick={() => {
                                        this.reverseVal()
                                    }}>+/-
                                    </button>
                                </div>

                                <div className='btn-item'>
                                    <button className='keypad-button-reset' onClick={() => {
                                        this.setClear()
                                    }}>C
                                    </button>
                                </div>


                                <div className='btn-item t-2'>
                                    <button className='keypad-button-reset t-2' onClick={() => {
                                        this.turnOnCalculator()
                                    }}>AC
                                    </button>

                                    <span className='turn-on-desc'>ON</span>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}


const mapStateToProps = (state) => ({
    calculatorData: state.calculatorReducer
});

export default connect(mapStateToProps)(HomePage);