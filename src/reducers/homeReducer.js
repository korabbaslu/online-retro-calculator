import {
    SET_NUM,
    SET_RESULT,
    SET_MEMORY,
    SET_FLOAT_TRUE,
    RESET_NUM,
    TURN_OFF_CALCULATOR,
    SET_AWAIT_NUM, REVERSE_NUMBER_ACTION, RESET_MEMORY_ACTION,
    TURN_ON_CALCULATOR, RELOAD_MEMORY

} from '../constants/actionTypes';

import * as CONSTS from './../constants/index';

const initialState = {
    firstNum: "",
    showNum: "",
    secondNum: null,
    result: 0,
    memory: 0,
    isFloat: false,
    operation: CONSTS.PLUS
};

const calculatorReducer = (state = initialState, action) => {

    switch (action.type) {

        case SET_NUM:
            let decimalNum = state.firstNum;
            if(state.isFloat===true){
                let splitedNum = decimalNum.split(".");
                if(splitedNum.length>1){
                    let smallPartNum = splitedNum[splitedNum.length-1]+action.payload.toString();
                    splitedNum[splitedNum.length-1] = smallPartNum;
                    decimalNum = splitedNum.join(".");
                }
                else{
                    decimalNum = decimalNum+"."+action.payload.toString();
                }
            }
            else{
                decimalNum = decimalNum + action.payload.toString()
            }

            let num = decimalNum.toString();
            return {
                ...state,
                firstNum: num,
                showNum: num
            };

        case SET_AWAIT_NUM:
            let resultNum = null;
            switch (state.payload) {
                case CONSTS.PLUS:

                    resultNum = parseFloat(state.result) + parseFloat(state.firstNum);
                    break;
                case CONSTS.MINUS:

                    resultNum = parseFloat(state.result) - parseFloat(state.firstNum);
                    break;
                case CONSTS.CROSS:

                    resultNum = parseFloat(state.result) * parseFloat(state.firstNum);
                    break;
                case CONSTS.DIVIDE:

                    resultNum = parseFloat(state.result) / parseFloat(state.firstNum);
                    break;
                default:
                    resultNum = parseFloat(state.result) + parseFloat(state.firstNum);
            }


            let resultObj = {};

            if (state.firstNum !== "") {
                resultObj = {
                    firstNum: "",
                    secondNum: state.firstNum,
                    result: resultNum,
                    showNum: resultNum
                }
            }
            return {
                ...state,
                ...resultObj,
                operation: action.payload
            };

        case SET_FLOAT_TRUE:
            return {
                ...state,
                isFloat: action.payload
            };


        case RESET_NUM:
            return {
                ...state,
                firstNum: "",
                showNum:"0",
                secondNum:""
            };


        case SET_MEMORY:
            let memory = null;
            switch (action.payload){
                case CONSTS.MINUS:
                    memory = -(parseFloat(state.showNum));
                    break;
                case CONSTS.PLUS:
                    memory = parseFloat(state.showNum);
                    break;
            }
            return {
                ...state,
                memory
            };

        case SET_RESULT:
            return {
                ...state,
                isFloat: false,
                result: action.payload.toString(),
                showNum: action.payload.toString(),
                firstNum: "",
                secondNum: ""
            };


        case TURN_OFF_CALCULATOR:
            return {
                ...initialState
            };


        case REVERSE_NUMBER_ACTION:

            let isShowNumNegative = Math.sign(state.showNum)===-1;
            let isResultNegative = Math.sign(state.result)===-1;

            let reverseShowNum = isShowNumNegative ? Math.abs(state.showNum) :-Math.abs(state.showNum);
            let reverseResult = isResultNegative ? Math.abs(state.result) :-Math.abs(state.result);

            return {
                ...state,
                showNum: reverseShowNum,
                result: reverseResult,
            };


        case RESET_MEMORY_ACTION:

            return {
                ...state,
                memory:0
            };


        case TURN_ON_CALCULATOR:

            return {
                ...initialState,
                memory:state.memory,
                showNum:"0"
            };

            case RELOAD_MEMORY:
                console.log("state.memory ",state.memory);
            return {
                ...state,
                showNum:state.memory,
                result:state.memory,
            };


        default:
            return state;
    }
};

export default calculatorReducer;