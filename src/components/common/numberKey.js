import React from 'react';

const NumberKey = ({num,setNum})=>{
    return <div className='btn-item'><button className='keypad-button' type='button' onClick={()=>{
        setNum(num)
    }}>{num}</button></div>
};


export default NumberKey;
