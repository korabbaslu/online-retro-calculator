import * as actionTypes from "../constants/actionTypes";
import {SET_MEMORY} from "../constants/actionTypes";

const setNumber = (num)=> ({
    type:actionTypes.SET_NUM,
    payload:num
});

export const setNumberAction = (num)=>dispatch =>{
    dispatch(setNumber(num))
}


const setFloatTrue = (boolVal)=> ({
    type:actionTypes.SET_FLOAT_TRUE,
    payload:boolVal
});

export const setFloatTrueAction = (boolVal)=>dispatch =>{
    dispatch(setFloatTrue(boolVal))
}


const resetCalc = ()=> ({
    type:actionTypes.RESET_NUM,
    payload:null
});

export const resetCalcAction = ()=>dispatch =>{
    dispatch(
        resetCalc()
    )
}


const turnOff = ()=> ({
    type:actionTypes.TURN_OFF_CALCULATOR,
    payload: null
});

export const turnOffAction = ()=>dispatch =>{
    dispatch(
        turnOff()
    )
};

const operationOn = (operation)=>({
    type:actionTypes.SET_AWAIT_NUM,
    payload:operation
});

export const operationOnAction = (operation)=>dispatch =>{
    dispatch(
        operationOn(operation)
    )
};

const setResult = (result)=>({
    type:actionTypes.SET_RESULT,
    payload:result
});

export const setResultAction = (result)=>dispatch =>{
    dispatch(
        setResult(result)
    )
};


const setMemory = (type)=>({
    type:actionTypes.SET_MEMORY,
    payload:type
});

export const setMemoryAction = (type)=>dispatch =>{
    dispatch(
        setMemory(type)
    )
};

const reverseNumber = ()=>({
    type:actionTypes.REVERSE_NUMBER_ACTION,
    payload:null
});

export const reverseNumberAction = ()=>dispatch =>{
    dispatch(
        reverseNumber()
    )
};

const resetMemory = ()=>({
    type:actionTypes.RESET_MEMORY_ACTION,
    payload:null
});

export const resetMemoryAction = ()=>dispatch =>{
    dispatch(
        resetMemory()
    )
};

const turnOnCalculator = ()=>({
    type:actionTypes.TURN_ON_CALCULATOR,
    payload:null
});

export const turnOnCalculatorAction = ()=>dispatch =>{
    dispatch(
        turnOnCalculator()
    )
};


const reloadMemory = ()=>({
    type:actionTypes.RELOAD_MEMORY,
    payload:null
});

export const reloadMemoryAction = ()=>dispatch =>{
    dispatch(
        reloadMemory()
    )
};