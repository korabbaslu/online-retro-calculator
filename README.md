`to install application:`

with npm:

npm install

with yarn:

yarn install

`after dependencies installed, in order to run application:`

**npm run start** or **yarn run start**
